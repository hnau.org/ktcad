package hnau.plugin.utils

import org.gradle.api.Project
import org.jetbrains.kotlin.gradle.dsl.kotlinExtension

fun Project.configSourceSets() {
    listOf("main", "test").forEach { sourceType ->
        kotlinExtension
            .sourceSets
            .getByName(sourceType)
            .kotlin { srcDir(sourceType) }
    }
}
