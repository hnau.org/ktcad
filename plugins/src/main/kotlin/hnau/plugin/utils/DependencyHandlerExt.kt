package hnau.plugin.utils

import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler

fun DependencyHandler.implementation(notation: Any): Dependency? =
    add("implementation", notation)
