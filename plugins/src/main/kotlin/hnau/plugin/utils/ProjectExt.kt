package hnau.plugin.utils

import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalog
import org.gradle.api.artifacts.VersionCatalogsExtension
import org.gradle.api.tasks.compile.JavaCompile
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

fun Project.getVersionCatalog(): VersionCatalog =
    extensions.get<VersionCatalogsExtension>().named("libs")

fun Project.setJavaVersionCompatibility() {
    val version = getVersionCatalog().getTargetJvmVersion()
    tasks.withType(JavaCompile::class.java) { javaCompile ->
        javaCompile.sourceCompatibility = version.majorVersion
        javaCompile.targetCompatibility = version.majorVersion
    }
    tasks.withType(KotlinCompile::class.java) { kotlinCompile ->
        kotlinCompile.kotlinOptions.jvmTarget = version.toString()
    }
}

val Project.namespace
    get() = "hnau." + path.drop(1).replace(':', '.')
