package hnau.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project

class HnauKotlinAppPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        target.plugins.apply {
            apply("hnau.kotlin")
            apply("application")
        }
    }
}
