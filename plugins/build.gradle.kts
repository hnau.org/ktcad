plugins {
    alias(libs.plugins.kotlin.jvm)
    id("java-gradle-plugin")
}

dependencies {
    implementation(libs.gradle.plugin.kotlin.jvm)
}

val javaVersion = JavaVersion.valueOf(libs.versions.java.get())
val javaVersionNumber = javaVersion.ordinal + 1

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(javaVersionNumber))
}

kotlin {
    jvmToolchain(javaVersionNumber)
}

gradlePlugin {
    plugins.create("Base kotlin module") {
        id = "hnau.kotlin"
        implementationClass = "hnau.plugin.HnauKotlinPlugin"
    }
    plugins.create("Kotlin library") {
        id = "hnau.kotlin.lib"
        implementationClass = "hnau.plugin.HnauKotlinLibPlugin"
    }
    plugins.create("Kotlin application") {
        id = "hnau.kotlin.app"
        implementationClass = "hnau.plugin.HnauKotlinAppPlugin"
    }
}
