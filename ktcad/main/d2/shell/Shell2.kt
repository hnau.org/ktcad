package d2.shell

import hnau.ktcad.d2.Segmented
import hnau.ktcad.d2.Vector2

fun interface Shell2 {

    suspend fun segmentate(
        axisMinSegments: Vector2,
    ): Segmented

    companion object
}