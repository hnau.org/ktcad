package d2.shell

import hnau.ktcad.common.Dimension
import hnau.ktcad.d2.Point2
import hnau.ktcad.d2.Segmented
import hnau.ktcad.d2.Vector2
import kotlin.math.PI
import kotlin.math.ceil
import kotlin.math.cos
import kotlin.math.sin

object Circle : Shell2 {

    override fun toShell(
        maxSegmentLength: Dimension,
    ): Segmented {
        val segments = ceil(PI * 2 / maxSegmentLength.value).toInt()
        val angleStep = PI * 2 / segments
        return Segmented(
            points = (0..segments).map { i ->
                val angle = angleStep * i
                val x = Dimension(cos(angle))
                val y = Dimension(sin(angle))
                Point2(x, y)
            },
        )
    }

    override suspend fun segmentate(
        axisMinSegments: Vector2,
    ): Segmented {
        
    }
}

val Shell2.Companion.circle: Circle
    get() = Circle