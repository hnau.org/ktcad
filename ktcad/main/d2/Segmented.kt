package hnau.ktcad.d2

data class Segmented(
    val points: List<Point2>,
)