package hnau.ktcad.d2

import hnau.ktcad.common.Dimension

data class Point2(
    val x: Dimension,
    val y: Dimension,
) {

    override fun equals(
        other: Any?,
    ): Boolean = other is Point2 &&
            x.isEqualTo(other.x) &&
            y.isEqualTo(other.y)

    override fun hashCode(): Int {
        var result = x.hash()
        result = 31 * result + y.hash()
        return result
    }
}