package hnau.ktcad.d2

import hnau.ktcad.common.Dimension

data class Vector2(
    val dx: Dimension,
    val dy: Dimension,
) {

    override fun equals(
        other: Any?,
    ): Boolean = other is Vector2 &&
            dx.isEqualTo(other.dx) &&
            dy.isEqualTo(other.dy)

    override fun hashCode(): Int {
        var result = dx.hash()
        result = 31 * result + dy.hash()
        return result
    }
}