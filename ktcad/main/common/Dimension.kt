package hnau.ktcad.common

import kotlin.math.abs

@JvmInline
value class Dimension(
    val value: Double,
) {

    fun isEqualTo(
        other: Dimension,
    ): Boolean {
        val delta = abs(value - other.value)
        return delta <= maxEqualsDelta.value
    }

    fun hash(): Int = (value / maxEqualsDelta.value).toLong().hashCode()

    companion object {

        val maxEqualsDelta: Dimension = Dimension(0.001)
    }
}