plugins {
    id("hnau.kotlin.lib")
}

dependencies {
    implementation(libs.arrow.core)
    implementation(libs.kotlin.coroutines.core)
}
